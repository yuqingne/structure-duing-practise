package com.duing.backTracking;

import java.util.Arrays;
import java.util.HashSet;

public class StringPermutation {

    public static void main(String[] args) {
        permutation("abc", 0, 2);
        System.out.println(Arrays.toString(set.toArray()));
    }

    public static HashSet set = new HashSet();

    public static void permutation(String str, int start, int end) {
        //出口
        if (start == end) {
            System.out.println(str);
            set.add(str);
        }

        // 字符数组
        char[] array = str.toCharArray();
        for (int i = start; i <= end; i++) {
            // 让每一个字符都可以成为前缀  先交换一次
            swap(array, start, i);
            permutation(String.valueOf(array), start + 1, end);
            //还原
            swap(array, start, i);

        }

    }

    public static void swap(char[] chars, int i, int j) {
        if (i == j) return;
        char tmp = chars[i];
        chars[i] = chars[j];
        chars[j] = tmp;
    }

}
