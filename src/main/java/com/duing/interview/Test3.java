package com.duing.interview;

import java.util.*;

public class Test3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] str = input.split(" ");
        int rechargeAmount = Integer.parseInt(str[0]);
        int packagePrice = Integer.parseInt(str[1]);
        int n = Integer.parseInt(str[2]);

        String[] arr = new String[3];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextLine();
        }
        int[][] policies = new int[n][3];
        for (int i = 0; i < n; i++) {
            String[] tmp = arr[i].split(" ");
            for (int j = 0; j < 3; j++) {
                policies[i][j] = Integer.parseInt(tmp[j]);
            }
        }

        int maxDuration = calculateMaxDuration(rechargeAmount, packagePrice, policies);
        System.out.println(maxDuration);
    }

    public static int calculateMaxDuration(int rechargeAmount, int packagePrice, int[][] policies) {
        int duration = rechargeAmount / packagePrice;
        int[] used = new int[duration + 1];

        for (int i = 0; i < policies.length; i++) {
            int minDuration = policies[i][0];
            int multiple = policies[i][1];
            int bonusDuration = policies[i][2];

            for (int j = duration; j >= minDuration; j--) {
                if (used[j - minDuration] < multiple) {
                    used[j] = Math.max(used[j], used[j - minDuration] + 1);
                }
            }

            for (int j = minDuration; j <= duration; j++) {
                if (j < used.length && used[j] > 0) {
                    duration = Math.max(duration, j + bonusDuration);
                }
            }
        }

        return duration;
    }
}
