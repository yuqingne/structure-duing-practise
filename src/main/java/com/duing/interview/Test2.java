package com.duing.interview;

import java.util.Arrays;
import java.util.Scanner;

public class Test2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] str = input.split(" ");
        String[] goodsStr = str[0].split(",");
        int[] goods = new int[goodsStr.length];
        for (int i = 0; i < goodsStr.length; i++) {
            goods[i] = Integer.parseInt(goodsStr[i]);
        }
        int h = Integer.parseInt(str[1]);

        int minSpeed = calculateMinTransportSpeed(goods, h);
        System.out.println(minSpeed);
    }

    public static int calculateMinTransportSpeed(int[] goods, int h) {
        int maxGoods = Arrays.stream(goods).max().getAsInt();
        int minSpeed = 1;
        while (true) {
            int hours = 0;
            for (int i = 0; i < goods.length; i++) {
                hours += (int) Math.ceil((double) goods[i] / minSpeed);
            }
            if (hours <= h) {
                return minSpeed;
            }
            minSpeed++;
        }
    }
}