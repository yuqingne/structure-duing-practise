package com.duing.array;

import java.util.Arrays;

public class RepeatNum {

    /**
     * 排序后重复元素相邻
     * [2, 3, 1, 0, 2, 5, 3]
     * [0, 1, 2, 2, 3, 3, 5]
     */
    public int findRepeatNumber(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                return nums[i];
            }
        }
        return -1;
    }
}
