package com.duing.array;

import student.duing.wangli.TestFromLi;

import java.util.Arrays;

public class MergeSorterArr {

    public static void main(String[] args) {
        int[] A = new int[]{1, 2, 3, 0, 0, 0};
        int[] B = new int[]{2, 5, 6};
        TestFromLi.merge4(A, 3, B, 3);
        //merge(A, 3, B, 3);
        System.out.println(Arrays.toString(A));
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        // 遍历nums1的指针  0 - (m-1)
        int pa = 0;
        // 遍历nums2的指针  0 - (n-1)
        int pb = 0;
        // 盛装结果
        int[] sorted = new int[m + n];

        // 两个数组中 剩余元素的最小值
        int cur;
        // A = [1,2,3,0,0,0]     B = [2,5,6]
        while (pa < m || pb < n) {
            if (pa == m) {
                cur = nums2[pb];
                pb++;
//                cur = nums2[pb++];  可以合起来写
            } else if (pb == n) {
                cur = nums1[pa];
                pa++;
            } else if (nums1[pa] <= nums2[pb]) {
                // 1 < 2     2 <= 2
                // cur = 1     cur = 2
                cur = nums1[pa];
                pa++;
                // pa = 1 , pb = 0
                // pa = 2 , pb = 0
            } else {
                cur = nums2[pb];
                pb++;
            }
            // 计算 取出来的值  在新数组中的存放位置
            //   是 取出时刻  pa+pb
            //   又因为  取出后 对应指针自增   最终的位置为 pa+pb-1
            sorted[pa + pb - 1] = cur;
        }

//        for (int i = 0; i < sorted.length; i++) {
//            nums1[i] = sorted[i];
//        }
        System.arraycopy(sorted, 0, nums1, 0, (m + n));
    }


    // 插入排序的解法
    public static void merge1(int[] nums1, int m, int[] nums2, int n) {
        //声明nums的初始指针为2  nums1的初始指针为0
        int pa = m - 1;
        int pb = 0;
        //遍历nums1将其中的元素依次放入nums中排序
        for (int i = 0; i < n; i++) {
            // pa<0 兼容 nums1的值为{0} 即m=0的情况
            if (pa < 0 || nums2[pb] >= nums1[pa]) {
                // 如果nums2中的值大  则将nums2放在这个值的后面
                nums1[pa + 1] = nums2[pb];
                pa++;
                pb++;
                continue;
            }

            // 如果nums1中的值大  则将nums2放在这个值的前面
            nums1[pa + 1] = nums1[pa];
            nums1[pa] = nums2[pb];

            for (int j = pa; j > 0; j--) {
                if (nums1[j] >= nums1[j - 1]) {
                    break;
                }
                // 如果比前一个值小  则前移
                int tmp = nums1[j - 1];
                nums1[j - 1] = nums1[j];
                nums1[j] = tmp;
            }

            pa++;
            pb++;
        }
    }
}
