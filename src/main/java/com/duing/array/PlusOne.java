package com.duing.array;

import java.util.Arrays;

public class PlusOne {

    public static void main(String[] args) {
        int[] arr = new int[]{9,9,9};
        System.out.println(Arrays.toString(plusOne(arr)));
    }

    /**
     * 首先  对数组  从后往前遍历
     * 进位的情况           9 + 1 = 10  % 10  =  0
     * 不进位的情况      0-8 + 1  <= 9  % 10  = 自身
     * 如果没进位   直接返回     如果有进位   继续循环
     * 如果最高位也进位了   需要创建新的数组
     *
     * @param digits
     * @return
     */
    public static int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            digits[i]++;
            digits[i] = digits[i] % 10;
            if (digits[i] != 0) {
                return digits;
            }
        }

        digits = new int[digits.length + 1];
        digits[0] = 1;
        return digits;
    }
}
