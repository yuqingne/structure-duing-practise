package com.duing.recu;

public class TestLogN {

    public static void main(String[] args) {
        //f(200);

        // ^
        // 5 ^ 1
        System.out.println(5 ^ 1);
        // 001  -> 1
        // 101  -> 5
        // 100  -> 4


        // [5,1,6,10,12]
        // 只有一个元素的子集  只有两个元素的子集 。。 n-1 n
        // [5] [1] [6] [10] [12]
        // [5,1] [5,6] [5,10] [5,12]
        //    [1,6] [1,10] [1,12]
        //      [6,10]  [6,12]
        //         [10,12]
        // [5,1,6] [5,1,10] [5,1,12]
        //   [5,6,10] [5,6,12]
        //      [5,10,12]

        // List<List<Integer>>
    }

    // O(logN)
    public static void f(int N) {
        // 以1行代码执行1颗粒  O(1)  O(n)

        // O(1)
        //int i = 0;
        //int j = 1;
        //int k = 2;

        // O(n)
        //for (int i = 0; i < N; i++) {
        //    System.out.println(i);
        //}

        int x = 0;
        int i = 1;
        while (i < N) {
            //i = i * 2;
            i = i * 2;
            System.out.println(i + " " + x);
            x++;
        }
        System.out.println(x);
        // x = log(2)N


    }
}
