package com.duing.listnode;

// 约瑟夫环
public class Josephus {

    public static void main(String[] args) {
        System.out.println(josephus1(5, 3));
        System.out.println("===============");
        System.out.println(josephus2(5, 3));
        //System.out.println(josephus1(5, 2));
    }

    public static int josephus1(int n, int m) {
        if (n == 1) return 0;
        int result = (josephus1(n - 1, m) + m) % n;
        System.out.printf("%d,%d %d",n,m,result);
        System.out.println();
        return result;
    }


    public static int josephus2(int n, int m) {
        if (n == 1) return 1;
        int result = (josephus2(n - 1, m) + m - 1) % n + 1;
        System.out.printf("%d,%d %d",n,m,result);
        System.out.println();
        return result;
    }
}
