package com.duing.listnode;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class LRUCache extends LinkedHashMap<Integer, Integer> {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        // 还有一种取值方式  可以设置默认值
        //map.getOrDefault(1, -1);


        //Map<Integer, Integer> newMap = new LinkedHashMap<>(10, 0.75f, true);
        //newMap.put(1, 1);
        //newMap.put(2, 2);
        //newMap.get(1);
        //newMap.put(3, 3);
        //newMap.get(2);
        //newMap.put(4, 4);
        //System.out.println();

        //ConcurrentHashMap
        //TreeMap
        //LinkedHashMap

        // 支持两种排序方式  按照插入顺序  或者按照读取顺序
        // 构造器  初始化容器的大小、负载因子、使用的排序方式
        // 还提供了  当实际使用大小到达容量阈值时  调用removeEldestEntry方法进行判断  是否移除元素
        //        可通过继承父类  重写此方法

        // 定义容器的通用规范  元素真正存储的数据结构  容量（最多可使用的大小）  实际使用大小


        LRUCache1 lRUCache = new LRUCache1(2);
        lRUCache.put(1, 1); // 缓存是 {1=1}
        lRUCache.put(2, 2); // 缓存是 {1=1, 2=2}
        int r1 = lRUCache.get(1);    // 返回 1
        System.out.println(r1);
        lRUCache.put(3, 3); // 该操作会使得关键字 2 作废，缓存是 {1=1, 3=3}
        int r2 = lRUCache.get(2);    // 返回 -1 (未找到)
        System.out.println(r2);
        lRUCache.put(4, 4); // 该操作会使得关键字 1 作废，缓存是 {4=4, 3=3}
        int r3 = lRUCache.get(1);    // 返回 -1 (未找到)
        int r4 = lRUCache.get(3);    // 返回 3
        int r5 = lRUCache.get(4);    // 返回 4
        System.out.printf("%d %d %d", r3, r4, r5);
        System.out.println();

    }

    private int capacity;

    public LRUCache(int capacity) {
        // 使用 按照读取顺序排序的容器
        super(capacity, 0.75f, true);
        this.capacity = capacity;
    }

    public int get(int key) {
        // 取不到数据时  设置默认值
        return super.getOrDefault(key, -1);
    }

    public void put(int key, int value) {
        super.put(key, value);
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
        // 使用超出了容量大小时  开始删除元素
        return size() > capacity;
    }
}
