//题目：给定一个整数数组 nums , 找到一个具有最大和的连续子数组（子数组最好包含一个元素），返回其最大和。
//暴力循环解法
public class SumOfSequence {
    public static void main(String[] args) {
        int[] nums ={-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(SumOfSequence(nums));
    }
    public static int SumOfSequence(int[] arr ){
        //新建一个数组newArr用来储存当前元素nums[i]与前一个元素nums[i-1]的最大值
        int[] newArr = new int[arr.length];
        newArr[0] = arr[0];

        for (int i = 1; i<arr.length; i++){
            if (arr[i]+newArr[i-1]>arr[i]){
               newArr[i] = arr[i]+newArr[i-1];
            }else {
                newArr[i] = arr[i];
            }
        }
        //设计一个变量max，用来记录数组newArr的最大值
        int max = 0;
        for(int value : newArr){
            if (value > max){
                max = value;
            }
        }
        return max;
    }
}
