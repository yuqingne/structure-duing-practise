package student.duing.wangli;

public class TestFromLi {

    /**
     * 方法五：合并有序数组----利用逆向双指针
     * 不创建新的数组
     * 只在nums1中处理
     */
    public static void merge4(int[] nums1, int m, int[] nums2, int n) {
        //nums1的指针
        int pa = m - 1;
        //nums2的指针
        int pb = n - 1;
        //遍历nums1
        for (int i = m + n - 1; i > 0; i--) {
            if (nums1[pa] >= nums2[pb]) {
                nums1[i] = nums1[pa];
                //前移
                pa--;
            } else {
                nums1[i] = nums2[pb];
                //前移
                pb--;
            }
        }
    }


}
